#!/usr/bin/env bash

cp lab2-configurations/leaf01/interfaces configurations/leaf01/
cp lab2-configurations/leaf02/interfaces configurations/leaf02/
cp lab2-configurations/server01/interfaces configurations/server01/
cp lab2-configurations/server02/interfaces configurations/server02/
